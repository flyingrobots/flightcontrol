// Import libraries for use in algorithm
var connect = require('net');
var arDrone = require('ar-drone');
var drone  = arDrone.createClient();

// disable emergency status if set
drone.disableEmergency();

// array for holding processed sensor data for use in the control algorithm
var distance = {'left90': 0, 'left': 0, 'center': 0, 'right' : 0, 'right90': 0};

// if all sensors are greater than the safe distance, good to move forward
var safeDist = 80;


// warn distance when moving forward
var frontWarnDist = 65;

// if left and right sensor safe distance
var sideSafeDist = 55;

// if left90 and right90 sensors are greater than this distance safe on sides
// do not need to adjust on the sides
var side90SafeDist = 40;

// when we begin rotating, store the beginning left and right distances here
var rightStart = 0;
var leftStart = 0;

// if the dimension rotating to gets this much closer
// on the sensor it is rotating towards, then stop
var rotationPadding = 0.85;

// multiply all movement commands by this speed to determine
// an overall agressiveness of the drones flight
var speedFactor = 0.4;

// variables for timing actions
var hoverStartTime;
var backStartTime;
var upStartTime;
var left90StartTime;
var right90StartTime;

// How long the drone should move up when it first takes off
var upTime = 2000;

// how long the drone has been moving to the left and right (90 degrees)
var left90Duration;
var right90Duration;

// boolean values determining if we are compensating for sudden movement to one side. 
var leftCorrecting;
var rightCorrecting;

// variables showing how fast and for how long the side 90 reaction is
var sideMovSpeed = 0.8;
var sideCorrSpeed = 0.3;
var sideMovTime = 150;
var sideCorrTime = 90;

// current state of the drone's flight
var state = 0;
var prevstate = 0;

// tolerance on sides
var startTolerance = 1;

// Tolerance to moving to each side
var leftTolerance = startTolerance;
var rightTolerance = startTolerance;
var toleranceAdjustment = 0.85;

// reset the tolerances of left and right movement. 
function resetTolerance () {
  leftTolerance = startTolerance;
  rightTolerance = startTolerance;
}

// Start moving forward at a defined speed and taper off acceleration for even velocity. 
var frontInitialSpeed =	0.2;
var frontSpeedTaper = 	0.0009;
var frontSpeedRecover = 0.002;
var frontSpeedMinimum = 0.09;
var frontSpeed = frontInitialSpeed;

// output information about flight
function printFlightInfo () {
  console.log(
    'ctrl, state: ' + state +
    ';\tl9:' + parseInt(distance['left90'], 10) +
    ';\tl: ' + parseInt(distance['left'], 10) +
    ';\tc: ' + parseInt(distance['center'], 10) +
    ';\tr: ' + parseInt(distance['right'], 10) +
    ';\tr90: ' + parseInt(distance['right90'], 10)
  );
}

// speeds are specified in 0-1, filter out values over 1 and under 0
function spdSet(speed) {
  if (speed > 1) {
    return 1;
  } else if (speed < 0) {
  	return 0;
  } else {
  	return speed;
  }
}

function isFrontSafe() {
  if ( // the center is above the safe distance
	  distance['center'] > safeDist
  	)
  { // start moving forward, way is clear
    return true;
  }
  return false
}

// check if side (45) are safe
function isSideSafe() {
  if (
  	distance['left'] > sideSafeDist &&
    distance['right'] > sideSafeDist
  	)
  {
    return true;
  }
  return false;
}

// check if side (90) are safe
function isSide90Safe() {
  if (
  	distance['left90'] > side90SafeDist &&
    distance['right90'] > side90SafeDist
  	)
  {
    return true;
  }
  return false
}
// Set that we are waiting during the first hover state
var initialHover = true;

// maintain state and control the drone
// setting previous state to state
// should always go before changing of state,
// and after checking state and previous state
function control () {
  // change the front speed
	if (state == 2) {
		if (frontSpeed > frontSpeedMinimum) {
			frontSpeed -= frontSpeedTaper;
		}
	} else {
		if (frontSpeed < frontInitialSpeed) {
			frontSpeed += frontSpeedRecover;
		}
	}
  switch(state) {
    case -1: // drone is done. cannot exit this state. Comes here after ctrl c issued
      if (state != prevstate) { // only call stop if not already stopped
      	drone.stop();
      	drone.land();
      	console.log('ABORT COMMAND ISSUED: LANDING IMMEDIATELY.');
      }
      prevstate = state;
      break;
    case 0: // landed, waiting to start once there are sensor readings
      prevstate = state;
      if (distance['left'] && distance['center'] && distance['right']) {
        state = 1;
        drone.takeoff();
        console.log('TAKEOFF COMMAND ISSUED');
        hoverStartTime = Date.now();
      }
      break;
    case 1: // hovering
      if (state != prevstate) { // only call stop if not already stopped
        drone.stop();
        console.log('STOP COMMAND ISSUED');
      }
      prevstate = state;
      // AFter takeoff, the drone will wait in the hover state
      if (Date.now() - hoverStartTime < 5000) {
        console.log('HOVER WAITING.');
        break;
      }
      if (initialHover) {
      	// transfer to up for upping
      	initialHover = false;
      	state = 6;
      } else if (!isSideSafe()) {
      	if (distance['left'] > distance['right']){
        	// rotate left if there is more space to the left
        	state = 3;
      	} else {
        	// rotate right if there is more space to the right
        	state = 4;
      	}
      } else if (
      	!isSide90Safe()
      ) {
      	if (distance['left90'] > distance['right90']){
      		state = 7; // move left
      	} else {
      		state = 8; // move right
      	}
      } else if ( // if all measurements are greater than the safe distances, move forward
     		isFrontSafe()
      ) { // start moving forward, way is clear
        state = 2;
      } else { // default is to rotate
      	if (distance['left'] > distance['right']){
        	// rotate left if there is more space to the left
        	state = 3;
      	} else {
        	// rotate right if there is more space to the right
        	state = 4;
      	}
      }
      break;
    case 2: // moving forward
      if (state != prevstate) {
      	// reset tolerances for spinning algorithm, moving forward.
        resetTolerance();
      }
      drone.front(spdSet(frontSpeed * speedFactor));
      console.log('FRONT COMMAND ISSUED; Speed ' + frontSpeed);
      prevstate = state;
      if (
      	!isFrontSafe()
      ) { // stop moving forward, way is blocked
        state = 5;
      } else if (!isSide90Safe() || !isSideSafe()) {
      	state = 1;
      }
      break;
    case 3: // rotating left
      if (state != prevstate) { // only call rotate if not already
        drone.counterClockwise(spdSet(2 * speedFactor));
        leftTolerance *= toleranceAdjustment;
        leftStart = distance['left'];
        console.log('ROTATE LEFT COMMAND ISSUED');
      }
      prevstate = state;
      if (
      	isFrontSafe() || !isSide90Safe()
      ) { // stop rotating, path is clear
        state = 1;
      } else if (distance['left'] < leftStart * leftTolerance) {
        // If we have rotated left to find more room, and we find
        // less room, stop rotating left, let state 1 decide which
        // way to rotate.
        state = 1;
      }
      break;
    case 4: // rotating right
      if (state != prevstate) { // only call rotate if not already
        drone.clockwise(spdSet(2 * speedFactor));
        rightTolerance *= toleranceAdjustment;
        rightStart = distance['right'];
        console.log('ROTATE RIGHT COMMAND ISSUED');
      }
      prevstate = state;
      if (
      	isFrontSafe() || !isSide90Safe()
      ) { // stop rotating, path is clear
        state = 1;
      } else if (distance['right'] < rightStart * rightTolerance) {
        state = 1;
      }
      break;
    case 5: // back
      if (state != prevstate) { // only call back if not already moving back
        drone.back(spdSet(0.9 * speedFactor));
        console.log('BACK COMMAND ISSUED');
				frontSpeed = frontInitialSpeed;
        backStartTime = Date.now();
      }
      prevstate = state;
      // move back for a defined amounnt of time
      if (Date.now() - backStartTime < 300) {
        console.log('BACK WAITING.');
        break;
      } else {
        // after a defined amount of time, transfer control
        // back to hover.
        state = 1;
      }
      break;
    case 6: // up command
      if (state != prevstate) { // only call back if not already moving back
        drone.up(spdSet(2.5 * speedFactor));
        console.log('UP COMMAND ISSUED');
        upStartTime = Date.now();
      }
      prevstate = state;
      // move up for a defined amounnt of time
      if (Date.now() - upStartTime < upTime) {
        console.log('UP WAITING.');
        break;
      } else {
        // after a defined amount of time, transfer control
        // back to hover.
        state = 1;
      }
      break;
    case 7: // left90 command
      if (state != prevstate) { // only call back if not already moving back
      	if (prevstate == 8) {
        	drone.left(spdSet(sideCorrSpeed * speedFactor));
        	left90Duration = sideCorrTime;
        	leftCorrecting = true;
      	} else {
        	drone.left(spdSet(sideMovSpeed * speedFactor));
        	left90Duration = sideMovTime;
        	leftCorrecting = false;
      	}
        console.log('LEFT 90 COMMAND ISSUED');
        left90StartTime = Date.now();
      }
      prevstate = state;
      // move left for a defined amounnt of time
      if (Date.now() - left90StartTime < left90Duration) {
        console.log('LEFT 90 WAITING.');
        break;
      } else {
        // after a defined amount of time, transfer control
        // back to hover.
        if (leftCorrecting) {
        	state = 1;
        } else {
        	state = 8;
        }
      }
      break;
    case 8: // right90 command
      if (state != prevstate) { // only call back if not already moving back
      	if (prevstate == 7) {
        	drone.right(spdSet(sideCorrSpeed * speedFactor));
        	right90Duration = sideCorrTime;
        	rightCorrecting = true;
      	} else {
        	drone.right(spdSet(sideMovSpeed * speedFactor));
        	right90Duration = sideMovTime;
        	rightCorrecting = false;
      	}
        console.log('RIGHT 90 COMMAND ISSUED');
        right90StartTime = Date.now();
      }
      prevstate = state;
      // move left for a defined amounnt of time
      if (Date.now() - right90StartTime < right90Duration) {
        console.log('RIGHT 90 WAITING.');
        break;
      } else {
        // after a defined amount of time, transfer control
        // back to hover.
        if (rightCorrecting) {
        	state = 1;
        } else {
        	state = 7;
        }
      }
      break;
    default:
      console.log ('UNKNOWN STATE: ' + state);
  }
}

// map key in json to a data source for this algorithm
var pingToSensor = {
	'ping0' : 'center',
	'ping1' : 'left',
	'ping2' : 'right',
	'ping3' : 'left90',
	'ping4' : 'right90'
}

// process incoming data
function processData (dataObject) {
	var pingVal;
	var sensor;
	for (var pinger in dataObject) {
		pingVal = parseInt(dataObject[pinger], 10);
  	if (pingVal === 0) {
    	pingVal = 500;
  	}
  	dataObject[pinger] = pingVal;
  	sensor = pingToSensor[pinger];
  	distance[sensor] = pingVal;
  }
}

// telnet into the drone
var telnetClient = connect.connect('23', '192.168.1.1');

var dataBuffer;
var newline = 0;
telnetClient.on('data', function(data) {
  data = ""+data;
  console.log('TELNET RECIEVED DATA: ' + data);
}).on('connect', function() {
  // when the connecttion is established, automatically execute
  // the udp node module to get the sensor data to network via udp packets
  console.log('Connected');
  telnetClient.write('killall node\n');
  telnetClient.write('node /data/video/udptest2.js > /dev/null &\n');
  telnetClient.write('exit\n');
}).on('end', function() {
  console.log('Disconnected');
});

// listen for udp packets
var PORT = 33333;
var HOST = '0.0.0.0';
var dgram = require('dgram');
var server = dgram.createSocket('udp4');
server.on('listening', function () {
  var address = server.address();
  console.log('UDP server listening on ' + address.address + ":" + address.port);
  server.setBroadcast(true)
  server.setMulticastTTL(128);
  server.addMembership('230.185.192.108');
});
server.on('message', function (message, remote) {
  data = ""+message;
  try {
    // if parsing the incoming data was successful,
    // pass it to the process data function and then call
    // the control algorithm to evaluate flight state
    processData(JSON.parse(data));
    control();
    printFlightInfo();
  } catch (e) {
    // catch when data cannot be parsed
     console.log('PARSE ERROR: ' + data);
  }
});
server.bind(PORT, HOST);

// on the first sigint, land the drone. On the second sigint, close the program. 
var numSigints = 0;

process.on('SIGINT', function() {
	numSigints++;
	if (numSigints == 2) {
  	console.log("\n2 SIGINTS caught: Killing process immediately." );
  	process.exit();
	}
  console.log( "\nSIGINT caught" );
  // land drone
  state = -1;
});
